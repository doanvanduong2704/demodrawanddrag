$(document).ready(function () {
	console.log($('#testData'))
	$('#testData').text('show');  
  const canvas = document.querySelector('#draw')
const ctx = canvas.getContext('2d')

ctx.fillStyle = 'red'

function draw(x, y) {
	const circle = new Path2D();
	circle.arc(x, y, 25, 0, 2 * Math.PI);
	ctx.fill(circle);
}

let isMouseDown = false;


canvas.addEventListener('mousedown', function(e){
	isMouseDown = true
})

canvas.addEventListener('mouseup', function(e) {
	isMouseDown = false
})

canvas.addEventListener('mousemove', function(e) {
	if (!isMouseDown) {
		return
	}

	const {
		clientX,
		clientY
	} = e
  const react = canvas.getBoundingClientRect();
  draw(clientX - react.left, clientY - react.top);
})

const colorPickers = [ ...document.querySelectorAll('.color-picker')];
colorPickers.forEach(colorPicker => {
	colorPicker.addEventListener('click', (e) => {
		ctx.fillStyle = e.target.style.backgroundColor
	})
})

document.querySelector('#btn-clear').addEventListener('click', (e) => {
	ctx.clearRect(0, 0, 600, 600);
})

var listStudent = []
document.querySelector('#addStudent').addEventListener('click', (e) => {
	const student = {
		name: document.querySelector('#studentName').value,
		age: Math.random() * 100
	}
	listStudent.push(student)
	var listStdUl = document.querySelector('.listStudents')
	var li = document.createElement('li')
	li.appendChild(document.createTextNode(`${ listStudent[listStudent.length - 1]['name'] } is ${ Math.floor(listStudent[listStudent.length - 1]['age']) }  Age` ))
	listStdUl.appendChild(li)
})

function Person(first, last, age, gender, interests) {
	this.name = {
		first,
		last
	};
	this.age = age;
	this.gender = gender;
	this.interests = interests
}

function Teacher(first, last, age, gender, interests, subject) {
	Person.call(this, first, last, age, gender, interests)
	this.subject = subject
}

Teacher.prototype.greeting = function() {
	alert('hello')
}


document.querySelector('#upload').addEventListener('click', (e) => {
	var progressEl = document.querySelector('#progress')
	var width = 1
	var id = setInterval(frame, 10)
	function frame() {
		if (width >= 100) {
			// clearInterval(id)
		} else {
			width++;
			progressEl.style.width = width + '%'
		}
	}
})

var intervalID = setInterval(myCallback, 2000, 'param1', 'param2')
function myCallback(a, b) {
	// console.log(a)
	// console.log(b)
}
// variable to store our intervalID
let nIntervId;

function changeColor() {
	console.log(nIntervId)
  // check if already an interval has been set up
  if (!nIntervId) {
    nIntervId = setInterval(flashText, 1000);
  }
}

function flashText() {
	console.log("vao1")
  const oElem = document.getElementById("my_box");
  if (oElem.className === "go") {
    oElem.className = "stop";
  } else {
    oElem.className = "go";
  }
}

function stopTextColor() {
  clearInterval(nIntervId);
  // release our intervalID from the variable
}

document.getElementById("start").addEventListener("click", changeColor);
document.getElementById("stop").addEventListener("click", stopTextColor);


const canvasDraw = document.querySelector('.draw')
const bodyEl = document.querySelector('body')
let rect = {}
let listRect = []
let lstTest = []
let element = null
let isMouseDownDraw = false
let isMouseDownDrag = false
let mouse = {
	x: 0,
	y: 0,
	startX: 0,
	startY: 0
}
let elposition = {
	pos1: 0,
	pos2: 0,
	pos3: 0,
	pos4: 0
}

let offset = [0, 0]
let isDown = false
// listRect = fetchRectLS()
// for(let i = 0; i < listRect.length; i++) {
// 	drawRec(listRect[i].startX, listRect[i].startY, listRect[i].w, listRect[i].h)
// }

console.log('element...', element)

window.addEventListener('mousedown', mousedown)

function mousedown(e) {
	window.addEventListener('mousemove', mousemove)
	window.addEventListener('mouseup', mouseup)
	isMouseDownDraw = true
	mouse.startX = e.pageX
	mouse.startY = e.pageY
	if (mouse.startX < canvasDraw.offsetLeft) {
		return
	}
	createRectangle()
	let containerStyle = getComputedStyle(canvasDraw);
	function mousemove(e) {
		setMousePositionByWindow()
		if (!isMouseDownDraw) {
			return
		}
		if (element !== null) {
			if (mouse.startX < canvasDraw.getBoundingClientRect().left || mouse.startX > canvasDraw.getBoundingClientRect().right) {
				return
			}
			if (mouse.startY < canvasDraw.getBoundingClientRect().top) {
				return
			}
			let containerStyle = getComputedStyle(canvasDraw);
			let elWidth = Math.abs(mouse.startX - mouse.x)
			let elHeight = Math.abs(mouse.y - mouse.startY)
			let elLeft = parseInt(element.style.left)
			let elTop = parseInt(element.style.top)
			let parentWidth = canvasDraw.getBoundingClientRect().width
			let parentHeight = canvasDraw.getBoundingClientRect().height
			let elBottom = parseInt(parentHeight) - ((mouse.y - mouse.startY < 0 ? mouse.startY : mouse.y) - parseInt(canvasDraw.offsetTop))
			let elRight = parseInt(parentWidth) - ((mouse.x - mouse.startX < 0 ? mouse.startX : mouse.x) - parseInt(canvasDraw.offsetLeft))
			if ((elWidth + elLeft) > parentWidth) {
				elWidth = parentWidth - elLeft;
			}
			if ((elWidth + elRight) > parentWidth) {
				elWidth = parentWidth - elRight
				elLeft = 0
			} else {
				elLeft = (mouse.x - mouse.startX < 0) ? (mouse.x - canvasDraw.offsetLeft) + 'px' : (mouse.startX - canvasDraw.offsetLeft) + 'px'
			}
			if ((elHeight + elTop) > parentHeight) {
				elHeight = parentHeight - elTop
			}
			if ((elBottom + elHeight) > parentHeight) {
				elHeight = parentHeight - elBottom
				elTop = 0
			} else {
				elTop = (mouse.y - mouse.startY < 0) ? (mouse.y - canvasDraw.offsetTop) + 'px' : (mouse.startY - canvasDraw.offsetTop) + 'px'
			}

			if ((elTop + elHeight) > parentHeight) {
				elHeight = parentHeight - elTop
			}

			element.style.width = elWidth + 'px'
			element.style.height = elHeight + 'px'
			element.style.left = elLeft
			element.style.top = elTop
		}
	}
	function mouseup(e) {
		isMouseDownDraw = false
		window.removeEventListener('mousemove', mousemove)
		window.removeEventListener('mouseup', mouseup)
	}
}

function createRectangle() {
	element = document.createElement('div')
	element.setAttribute("id", makeid(5));
	element.className = 'rectangle'
	element.style.left = mouse.startX + 'px'
	element.style.top = mouse.startY + 'px'
	element.addEventListener('mousedown', mousedown)
	function mousedown(e) {	
		e = e || window.event
		e.stopPropagation()
		e.preventDefault()
		elposition.pos3 = e.pageX;
		elposition.pos4 = e.pageY;
		isDown = true
		window.addEventListener('mousemove', mousemove, true)
		window.addEventListener('mouseup', mouseup)
		function mousemove(e) {
			e = e || window.event;
			e.preventDefault();
			e.stopPropagation();
			elposition.pos1 = elposition.pos3 - e.pageX;
			elposition.pos2 = elposition.pos4 - e.pageY;
			elposition.pos3 = e.pageX;
			elposition.pos4 = e.pageY
			element.style.top
		}
		function mouseup(e) {
			e.stopPropagation()
			e.preventDefault()
			isDown = false
			window.removeEventListener('mousemove', mousemove)
			window.removeEventListener('mouseup', mouseup)
		}
		function elementDrag(e) {
			e = e || window.event;
			elposition.pos1 = elposition.pos3 - e.pageX;
			elposition.pos2 = elposition.pos4 - e.pageY;
			elposition.pos3 = e.pageX;
			elposition.pos4 = e.pageY;
			e.target.style.top = (e.target.offsetTop - elposition.pos2) + "px";
			e.target.style.left = (e.target.offsetLeft - elposition.pos1) + "px"
		}
	}
	canvasDraw.appendChild(element)
}

function setMousePositionByWindow(e) {
	var ev = e || window.event;
	if (ev.pageX) {
		mouse.x = ev.pageX
		mouse.y = ev.pageY
	} else if (ev.clientX) {
		mouse.x = ev.clientX + document.body.scrollLeft
		mouse.y = ev.clientY + document.body.scrollTop
	}
}

// window.addEventListener('mousemove', (e) => {
// })

// window.addEventListener('mouseup', (e) => {
// 	console.log('mouseupWindown', 'mouseupWindown')
// 	isMouseDownDraw = false
// 	isMouseDownDrag = false
// })


// canvasDraw.addEventListener('mousedown', function(e) {
// 	mouse.startX = mouse.x;
// 	mouse.startY = mouse.y;
// 	element = document.createElement('div')
// 	element.setAttribute("id", makeid(5));
// 	element.className = 'rectangle'
// 	element.i
// 	element.style.left = mouse.x + 'px'
// 	element.style.top = mouse.y + 'px'
// 	isMouseDownDraw = true
// 	element.addEventListener('mousedown', (e) => {
// 		if (!isMouseDownDraw) {
// 			e = e || window.event
// 			e.stopPropagation();
// 			e.preventDefault();
// 			elposition.pos3 = e.pageX;
// 			elposition.pos4 = e.pageY;
// 			e.target.addEventListener('mousemove', moveFunc)
// 		  e.target.addEventListener('mouseup', (e2) => {
// 		  	e2.target.removeEventListener('mousemove', (e1) => {
// 				e1.stopPropagation();
// 				e1.preventDefault();
// 				elementDrag(e1);
// 			})
// 		  })
// 		  e.target.addEventListener('mouseup', (e2) => {
// 		  	e.target.removeEventListener('mousemove', moveFunc)
// 		  })
// 		}
// 	})
// 	// element.addEventListener('mousemove', (e) => {
// 	// 	if (!isMouseDownDraw) {
// 	// 		e = e || window.event;
// 	// 		e.stopPropagation();
// 	// 		e.preventDefault();
			
// 	// 	}
// 	// })
// 	element.addEventListener('mouseup', (e) => {
// 		if (!isMouseDownDraw) {
// 		}
// 	})
// 	canvasDraw.appendChild(element)
// })


// canvasDraw.addEventListener('mousemove', function(e) {
// 	setMousePosition(e);
// 	if (!isMouseDownDraw) {
// 		return
// 	}
// 	if (element !== null) {
// 		element.style.width = Math.abs(mouse.x - mouse.startX) + 'px'
// 		element.style.height = Math.abs(mouse.y - mouse.startY) + 'px'
// 		element.style.left = (mouse.x - mouse.startX < 0) ? (mouse.x - this.offsetLeft) + 'px' : (mouse.startX - this.offsetLeft) + 'px'
// 		element.style.top = (mouse.y - mouse.startY < 0) ? (mouse.y -  this.offsetTop) + 'px' : (mouse.startY - this.offsetTop) + 'px'
// 	}
// 	// rect.w = (e.pageX - this.offsetLeft) - rect.startX
// 	// rect.h = (e.pageY - this.offsetTop) - rect.startY
// 	// ctxDraw.clearRect(0, 0, canvasDraw.width, canvasDraw.height)
// 	// for(let i = 0; i < listRect.length; i++) {
// 	// 	drawRec(listRect[i].startX, listRect[i].startY, listRect[i].w, listRect[i].h)
// 	// }
// 	// drawRec(rect.startX, rect.startY, rect.w, rect.h)
// })


// canvasDraw.addEventListener('mouseup', function(e) {
// 	isMouseDownDraw = false
// 	if (element !== null) {
// 		element = null
// 	}
// 	// updateRectLS()
// 	// listRect = fetchRectLS()
// 	// ctxDraw.clearRect(0, 0, canvasDraw.width, canvasDraw.height)
// })


function drawRec(x, y, w, h) {
	var rectangle = new Path2D();
	rectangle.rect(x, y, w, h);
	ctxDraw.stroke(rectangle);
}

function updateRectLS() {
	listRect.push(rect)
	localStorage.setItem("listRect", JSON.stringify(listRect))
}

function fetchRectLS() {
	return JSON.parse(localStorage.getItem("listRect")) == null ? [] : JSON.parse(localStorage.getItem("listRect"))
}

function setMousePosition(e) {
	var ev = e || window.event;
	if (ev.pageX) {
		mouse.x = ev.pageX
		mouse.y = ev.pageY
	} else if (ev.clientX) {
		mouse.x = ev.clientX + document.body.scrollLeft
		mouse.y = ev.clientY + document.body.scrollTop
	}
}

function makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * 
 charactersLength));
   }
   return result;
}

});
